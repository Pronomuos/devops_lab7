from brownian_motion import BrownianMotion

if __name__ == '__main__':
    hydrogen = BrownianMotion(temp=375, nu=1.04e-5,
                              r=1.25e-10, time=60, intervals=150, m=3.3e-27)
    print("Коэффициент диффузии = ", hydrogen.find_diffusion(), " м^2/с.")
    print("Средняя длина свободного пробега = ",
          hydrogen.find_free_path(), " см.")
    boltzmann = 0
    for i in range(0, 5):
        hydrogen.plot_trajectory()
        boltzmann += hydrogen.calc_boltzmann()
    print("Постоянная Больцмана = ", boltzmann / 5)
